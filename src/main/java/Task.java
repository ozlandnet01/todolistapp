import java.util.HashMap;

public class Task {
    private int taskID;
    private int taskStatus;
    private String taskName;
    private static HashMap<Integer, String> statusNames = new HashMap<Integer, String>() {{
        put(1, "DONE");
        put(2, "NOT DONE");
    }};

    public Task(int taskID, String taskName, int taskStatus) {
        this.taskID = taskID;
        this.taskName = taskName;
        this.taskStatus = taskStatus;
    }

    @Override
    public String toString() {
        return String.format("%d. %s [%s]", taskID, taskName, statusNames.get(taskStatus));
    }

    public int getTaskID() {
        return taskID;
    }

    public int switchStatus(){
        this.taskStatus = this.taskStatus  == 1 ? 2 : 1;
        return this.taskStatus;
    }


}
