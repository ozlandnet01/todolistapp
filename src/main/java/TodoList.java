import java.util.ArrayList;

public class TodoList {
    private ArrayList<Task> taskList;

    public TodoList() {
        taskList = new ArrayList<>();
    }

    public void addTask(Task task) {
        taskList.add(task);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Task task : taskList) {
            sb.append(String.format("%s\n", task.toString()));
        }
        
        return sb.toString();
    }

    public Task switchTaskStatus(int taskID){
        for (Task task : this.taskList ) {
            if(task.getTaskID() == taskID){
                task.switchStatus();
                return task;
            }
        }
        return null;
    }



}
