import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void toStringTaskDone() {
        String expected = "1. Make Test [DONE]";
        Task task = new Task(1, "Make Test", 1);

        assertEquals(expected, task.toString());
    }

    @Test
    public void toStringTaskNotDone() {
        String expected = "1. Make Test [NOT DONE]";
        Task task = new Task(1, "Make Test", 2);

        assertEquals(expected, task.toString());
    }

    @Test
    public void changeTaskStatus(){
        int expectedStatus = 1;
        Task task = new Task(1, "Make Test", 2);

        assertEquals(1,task.switchStatus());
    }
}