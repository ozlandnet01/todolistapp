import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {
    @Test
    public void toStringTodoListEmpty() {
        String expected = "";
        TodoList todoList = new TodoList();

        assertEquals(expected, todoList.toString());
    }

    @Test
    public void toStringTodoListNotEmpty() {
        String expected = "1. Make Test [DONE]\n2. Do Testing [NOT DONE]\n3. Learn TDD [NOT DONE]\n";
        TodoList todoList = new TodoList();
        todoList.addTask(new Task(1, "Make Test", 1));
        todoList.addTask(new Task(2, "Do Testing", 2));
        todoList.addTask(new Task(3, "Learn TDD", 2));

        assertEquals(expected, todoList.toString());
    }

    @Test
    public void givenTodoListWithTasks_whenStatusChanged_returnObjectWithNewStatus(){
        String expected = "1. Make Test [DONE]\n2. Do Testing [NOT DONE]\n3. Learn TDD [NOT DONE]\n";
        TodoList todoList = new TodoList();
        Task taskTest1 = new Task(1, "Make Test", 1);
        Task taskTest2 = new Task(2, "Do Testing", 2);
        Task taskTest3 = new Task(3, "Learn TDD", 2);
        todoList.addTask(taskTest1);
        todoList.addTask(taskTest2);
        todoList.addTask(taskTest3);

        assertEquals(taskTest2,todoList.switchTaskStatus(2));
    }
}